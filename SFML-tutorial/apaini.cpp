#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <conio.h>
#include <string>

using namespace std;

int main()
{
	sf::RenderWindow window(sf::VideoMode(540, 700), "SUDOKU", sf::Style::Close | sf::Style::Titlebar);
	sf::RectangleShape board(sf::Vector2f(540.0f, 540.0f));
	board.setPosition(sf::Vector2f(-2.0f, 160.0f));
	sf::Texture boardTexture;
	boardTexture.loadFromFile("Resource/InkedBoard.jpg");
	board.setTexture(&boardTexture);

	sf::RectangleShape indicator(sf::Vector2f(55.0f, 55.0f));
	indicator.setFillColor(sf::Color::Transparent);

	
	int gridX = 0;
	int gridY = 0;
	vector<vector<int>> gridArray(9);
	for(int i = 0 ; i<9 ; i++)
	{
		gridArray[i] = vector<int>(9);
		for(int j = 0 ; j<9 ; j++)
			gridArray[i][j] = 0;
		
	}
	/*for(int i = 0 ; i<9 ; i++)
	{
		for(int j = 0 ; j<9 ; j++)
			cout<<gridArray[i][j]<<" ";
		cout<<endl;
	}*/
	
	vector<vector<sf::Text>> text(9);
	sf::Font font;
	char temp[9];
	sf::Text coba;
	if (!font.loadFromFile("Star_Jedi_Rounded.ttf"))
	{
		cout<<"Load Font Error\n";
	}
	
	for(int i = 0 ; i<9 ; i++)
	{
		text[i] = vector<sf::Text>(9);
		for(int j = 0 ; j<9 ; j++)
		{
			text[i][j].setFont(font);
			text[i][j].setColor(sf::Color::Blue);
			text[i][j].setCharacterSize(40);
		}
	}
	
	coba.setFont(font);
	coba.setPosition(60.0f, 60.0f);
	coba.setCharacterSize(30);
	coba.setColor(sf::Color::Red);
	coba.setString("COBA");

	srand(time(0));
	int menu,grid[9][9],col = rand()%9,row = rand()%9;
	int region[9][9] = {
		{1,1,2,2,2,2,2,3,3},
		{1,1,1,2,2,2,3,3,3},
		{4,1,1,1,2,3,3,3,5},
		{4,4,1,6,6,6,3,5,5},
		{4,4,4,6,6,6,5,5,5},
		{4,4,7,6,6,6,8,5,5},
		{4,7,7,7,9,8,8,8,5},
		{7,7,7,9,9,9,8,8,8},
		{7,7,9,9,9,9,9,8,8}
	};
	string line[9];
	ifstream file;
	int count=0;
	file.open("sudoku.txt", ios::in);
	if(file.is_open())
	{
		while(getline(file,line[count]))
		{
			for(int i = 0 ; i<9 ; i++)
				gridArray[count][i] = line[count][i]-'0';
			cout<<endl;
			count++;
		}
		file.close();
	}
	for(int i = 0 ; i<9 ; i++)
	{
		for(int j = 0 ; j<9 ; j++)
			cout<<gridArray[i][j]<<" ";
		cout<<endl;
	}
	cout<<gridArray[3][0];

	
    while (window.isOpen())
    {
        sf::Event evnt;
        while (window.pollEvent(evnt))
        {
            if (evnt.type == evnt.Closed)
                window.close();
        }

		if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			//indicator.setFillColor(sf::Color::Red);
			indicator.setOutlineThickness(5);
			indicator.setOutlineColor(sf::Color::Blue);
			sf::Vector2i mousePos = sf::Mouse::getPosition(window);
			//cout<<mousePos.x<<endl;
			//cout<<mousePos.y<<endl;
			gridX = mousePos.x / 60;
			gridY = (mousePos.y - board.getPosition().y) / 60;
			cout<<gridX<<" , "<<gridY<<endl;
			indicator.setPosition((float(gridX)*60.0f)+0.0f, (float(gridY)*60.0f)+160.0f);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
		{
			gridArray[gridX][gridY] = 1;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
		{
			gridArray[gridX][gridY] = 2;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
		{
			gridArray[gridX][gridY] = 3;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
		{
			gridArray[gridX][gridY] = 4;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
		{
			gridArray[gridX][gridY] = 5;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
		{
			gridArray[gridX][gridY] = 6;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
		{
			gridArray[gridX][gridY] = 7;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
		{
			gridArray[gridX][gridY] = 8;
		}
		else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
		{
			gridArray[gridX][gridY] = 9;
		}

		for(int i = 0 ; i<9 ; i++)
		{
			for(int j = 0 ; j<9 ; j++)
			{
				if(gridArray[i][j] != 0)
				{
					text[i][j].setPosition((float(i)*60.0f)+15.0f, (float(j)*60.0f)+170.0f);
					text[i][j].setString(itoa(gridArray[i][j], temp, 10));
					
				}
			}
		}

		

        window.clear();
		window.draw(board);
		window.draw(indicator);
		for(int i = 0 ; i<9 ; i++)
		{
			for(int j = 0 ; j<9 ; j++)
			{
				window.draw(text[i][j]);
			}
		}
		window.draw(coba);
        window.display();

    }

    return 0;
}
