#include <SFML/Graphics.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <Windows.h>
#include <conio.h>
#include <string>

using namespace std;

void gotoxy(int x, int y)
{
	HANDLE hConsoleOutput;
	COORD dwCursorPosition;
	dwCursorPosition.X = x;
	dwCursorPosition.Y = y;
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleCursorPosition(hConsoleOutput,dwCursorPosition);
}

//fungsi buat cek horizontal, kalo ada yg sama dgn angka sekarang return true
bool used_horizontal(vector<vector<int>> grid , int row , int col, int num)
{
	for(int i = 0 ; i < 9 ; i++)
	{
		//cout<<"ROW: "<<row<<endl;
		if(i != col)
		{
			//cout<<grid[row][i]<<endl;
			if(grid[row][i] == num)
			{
				//cout<<grid[row][i]<<"AAAA"<<endl;
				return true;
			}
		}
		
	}
	return false;
}

//fungsi buat cek vertikal, kalo ada yg sama dgn angka sekarang return true
bool used_vertical(vector<vector<int>> grid , int row , int col, int num)
{
	for(int i = 0 ; i < 9 ; i++)
	{
		if(i != row)
		{
			if(grid[i][col] == num)
			{
				return true;
			}
		}
		
	}
	return false;
}

//fungsi buat cek region
bool used_region(vector<vector<int>> grid, int region[9][9], int row , int col , int num)
{
	//nyimpen region dari angka skrg ke variabel
	int check_region = region[row][col];
	for(int i = 0 ; i < 9 ; i++)
	{
		for(int j = 0 ; j < 9 ; j++)
		{
			//kalau region kotak yg ditelusuri sama dgn region angka sekarang, cek seperti cek horizontal/vertikal
			if(row != i && col != i)
			{
				if(region[i][j] == check_region)
				{
					if(grid[i][j] == num && i != row && j != col)
					{
						return true;
					}
				}
			}
			
		}
	}
	return false;
}

//cari kotak yang valuenya 0, kalo ketemu row col diset sesuai koordinat kotaknya
void findunassigned(vector<vector<int>> grid, int &row , int &col)
{
	//Kalo mau nyamping ganti i dan j
	for(int i = 0 ; i < 9 ; i++)
	{
		for(int j = 0 ; j < 9 ; j++)
		{
			if(grid[i][j] == 0)
			{
				row = i;
				col = j;
				goto end;
			}
		}
	}
	end:;
}

//cek apa semua kotak sudah terisi (valuenya bkn 0), kalau sudah return false
bool zerofound(vector<vector<int>> grid)
{
	for(int i = 0 ; i < 9 ; i++)
	{
		for(int j = 0 ; j < 9 ; j++)
		{
			if(grid[i][j] == 0)
			{
				return true;
			}
		}
	}
	return false;
}

void print(vector<vector<int>> grid)
{
	for(int i = 0 ; i < 9 ; i++)
	{
		for(int j = 0 ; j < 9 ; j++)
		{
			cout<<grid[i][j]<<" ";
		}
		cout<<endl;
	}
}

bool solve(vector<vector<int>> grid,int region[9][9],int row , int col, char* temp, vector<vector<sf::Text>>& text, sf::Text quit, sf::RectangleShape board, sf::RenderWindow& window)
{
	sf::Event event;
	if (window.pollEvent(event))
	{
		if (event.type == event.Closed)
					window.close();
	}
	//masukin angka ke kotak mulai 1-9
	for(int i = 1 ; i <= 9 ; i++)
	{
		//kalo ga ada 0 berarti selesai
		if(!zerofound(grid))
		{
			return true;
		}

		//nyari kotak yg bernilai 0
		findunassigned(grid,row,col);

		//cek horizontal + vertikal + region, 1 gak valid ganti angka berikutnya
		if(!used_horizontal(grid,row,col,i) && !used_vertical(grid,row,col,i) && !used_region(grid,region,row,col,i))
		{
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{
				return false;
			}

			grid[row][col] = i;
			print(grid);
			cout<<endl;
			system("CLS");

			text[row][col].setPosition((float(row)*60.0f)+15.0f, (float(col)*60.0f)+170.0f);
			text[row][col].setString(itoa(grid[row][col], temp, 10));
			text[row][col].setColor(sf::Color::Blue);
	
			window.clear();
			window.draw(board);
			for(int i = 0 ; i<9 ; i++)
			{
				for(int j = 0 ; j<9 ; j++)
				{
					if(grid[i][j] != 0)
						window.draw(text[i][j]);
				}
			}
			window.draw(quit);
			
			window.display();
			
			//fungsi backtrack
			if(solve(grid,region,row,col,temp,text,quit,board,window))
				return true;

			//habis dibacktrack valuenya dikembalikan jadi 0
			grid[row][col] = 0;

			text[row][col].setPosition((float(row)*60.0f)+15.0f, (float(col)*60.0f)+170.0f);
			text[row][col].setString(itoa(grid[row][col], temp, 10));
		}
	}
	//return false ketika 1-9 tidak valid semua dlm kotak tertentu, mentrigger backtracking
	return false;
	
	
}


int main()
{
	sf::RenderWindow window(sf::VideoMode(540, 700), "SUDOKU", sf::Style::Close | sf::Style::Titlebar);
	sf::RectangleShape board(sf::Vector2f(540.0f, 540.0f));
	board.setPosition(sf::Vector2f(-2.0f, 160.0f));
	sf::Texture boardTexture;
	boardTexture.loadFromFile("Resource/InkedBoard.jpg");
	board.setTexture(&boardTexture);

	sf::RectangleShape indicator(sf::Vector2f(55.0f, 55.0f));
	indicator.setFillColor(sf::Color::Transparent);

	int gridX = 0;
	int gridY = 0;
	vector<vector<int>> gridArray(9);
	for(int i = 0 ; i<9 ; i++)
	{
		gridArray[i] = vector<int>(9);
		for(int j = 0 ; j<9 ; j++)
			gridArray[i][j] = 0;
		
	}
	/*for(int i = 0 ; i<9 ; i++)
	{
		for(int j = 0 ; j<9 ; j++)
			cout<<gridArray[i][j]<<" ";
		cout<<endl;
	}*/

	vector<vector<sf::Text>> text(9);
	sf::Font font, font2;
	char temp[9];

	sf::Text coba;
	coba.setFont(font2);
	coba.setPosition(60.0f, 60.0f);
	coba.setCharacterSize(30);
	coba.setColor(sf::Color::Red);
	coba.setString("CONGRATULATIONS");

	sf::Text quit;
	quit.setFont(font);
	quit.setPosition(90.0f, 60.0f);
	quit.setCharacterSize(30);
	quit.setColor(sf::Color::Red);
	quit.setString("Press ESC to quit");

	if (!font2.loadFromFile("Xenotron.ttf"))
	{
		cout<<"Load Font Error\n";
	}

	if (!font.loadFromFile("Star_Jedi_Rounded.ttf"))
	{
		cout<<"Load Font Error\n";
	}

	for(int i = 0 ; i<9 ; i++)
	{
		text[i] = vector<sf::Text>(9);
		for(int j = 0 ; j<9 ; j++)
		{
			text[i][j].setFont(font);
			text[i][j].setColor(sf::Color::Blue);
			text[i][j].setCharacterSize(40);
		}
	}
	
	

	srand(time(0));
	int menu,grid[9][9],col = rand()%9,row = rand()%9;
	int region[9][9] = {
		{1,1,2,2,2,2,2,3,3},
		{1,1,1,2,2,2,3,3,3},
		{4,1,1,1,2,3,3,3,5},
		{4,4,1,6,6,6,3,5,5},
		{4,4,4,6,6,6,5,5,5},
		{4,4,7,6,6,6,8,5,5},
		{4,7,7,7,9,8,8,8,5},
		{7,7,7,9,9,9,8,8,8},
		{7,7,9,9,9,9,9,8,8}
	};
	
	

	menu:
	{
		//untuk Text
		sf::Text font,men1,men2,men3,men4,opt,opt1,opt2,mus1,mus2;
	
		//untuk Font
		sf::Font _x, _y;

		//untuk Texture
		sf::Texture a, _back;
		a.setSmooth(true);
		_back.setSmooth(true);

		//untuk load gambar panah
		if (!a.loadFromFile("arrow.png"))
		{
			// error...
			cout<<"Error\n";
		}

		//untuk load font Judul
		if (!_x.loadFromFile("Xenotron.ttf"))
		{
			// error...
			cout<<"Error\n";
		}

		//untuk load font Menu
		if (!_y.loadFromFile("X-Files.ttf"))
		{
			// error...
			cout<<"Error\n";
		}

		//untuk sprite
		sf::Sprite _b, _background;
		_background.setTexture(_back);
		_background.setScale(1.8f,1.8f);
		_b.setTexture(a);
		_b.setScale(0.05f,0.05f);
		_b.setPosition(140,205);

		//Main Menu
		//untuk set Judul Game
		font.setFont(_x);
		font.setCharacterSize(30);
		font.setString("SUDOKU");
		font.setColor(sf::Color::Red);
		font.setPosition(170,100);

		//untuk set menu Play
		men1.setFont(_y);
		men1.setCharacterSize(24);
		men1.setString("Play");
		men1.setColor(sf::Color::White);
		men1.setPosition(170,200);

		//untuk set menu Solve Random
		men2.setFont(_y);
		men2.setCharacterSize(24);
		men2.setString("Solve Random");
		men2.setColor(sf::Color::Red);
		men2.setPosition(170,230);

		//untuk set menu Solve Special
		men3.setFont(_y);
		men3.setCharacterSize(24);
		men3.setString("Solve Special");
		men3.setColor(sf::Color::Red);
		men3.setPosition(170,260);

		//untuk set menu EXIt
		men4.setFont(_y);
		men4.setCharacterSize(24);
		men4.setString("Exit");
		men4.setColor(sf::Color::Red);
		men4.setPosition(170,290);

		//untuk kembali ke main menu
		opt2.setFont(_y);
		opt2.setCharacterSize(24);
		opt2.setString("Back To Menu");
		opt2.setColor(sf::Color::Red);
		opt2.setPosition(170,230);

		int pos=1,posisix=0;
		while (window.isOpen())
		{
		
			sf::Event event;
			while (window.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					window.close();
			}
			if(posisix==0)
			{
				posisix*=-1;
			}
			if(posisix==100)
			{
				posisix*=-1;
			}
			posisix+=1;
			_background.setPosition(-100+posisix,-10);
		
			window.clear();
			window.draw(_background);
		
			window.draw(_b);
			window.draw(font);
			window.draw(men1);
			window.draw(men2);
			window.draw(men3);
			window.draw(men4);
		
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				if(pos==2)
				{
					pos=1;
					window.clear();
					men1.setColor(sf::Color::White);
					men2.setColor(sf::Color::Red);
					window.draw(_background);
					window.draw(font);
					window.draw(men1);
					window.draw(men2);
					window.draw(men3);
					window.draw(men4);
					_b.setPosition(140,205);
					window.draw(_b);

				}
				else if(pos==3)
				{
					pos=2;
					window.clear();
					window.draw(_background);
					men2.setColor(sf::Color::White);
					men3.setColor(sf::Color::Red);

					window.draw(font);
					window.draw(men1);
					window.draw(men2);
					window.draw(men3);
					window.draw(men4);
					_b.setPosition(140,235);
					window.draw(_b);
				}
				else if(pos==4)
				{
					pos=3;
					window.clear();
					window.draw(_background);
					men3.setColor(sf::Color::White);
					men4.setColor(sf::Color::Red);
					window.draw(font);
					window.draw(men1);
					window.draw(men2);
					window.draw(men3);
					window.draw(men4);
					_b.setPosition(140,265);
					window.draw(_b);
				}
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				if(pos==1)
				{
					pos=2;
					window.clear();
					window.draw(_background);
					men2.setColor(sf::Color::White);
					men1.setColor(sf::Color::Red);
					window.draw(font);
					window.draw(men1);
					window.draw(men2);
					window.draw(men3);
					window.draw(men4);
					_b.setPosition(140,235);
					window.draw(_b);
				}
				else if(pos==2)
				{
					pos=3;
					window.clear();
					window.draw(_background);
					men3.setColor(sf::Color::White);
					men2.setColor(sf::Color::Red);
					window.draw(font);
					window.draw(men1);
					window.draw(men2);
					window.draw(men3);
					window.draw(men4);

					_b.setPosition(140,265);
					window.draw(_b);
				}
				else if(pos==3)
				{
					pos=4;
					window.clear();
					window.draw(_background);
					men4.setColor(sf::Color::White);
					men3.setColor(sf::Color::Red);
					window.draw(font);
					window.draw(men1);
					window.draw(men2);
					window.draw(men3);
					window.draw(men4);
					_b.setPosition(140,295);
					window.draw(_b);
				}
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space )&&pos==1)
			{
				goto game;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space )&&pos==2)
			{
				goto solveRandom;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space )&&pos==3)
			{
				goto solveSpecial;
			}
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space )&&pos==4)
			{
				window.close();
			}
			window.display();
			Sleep(100);
		}
	}

game:
	{
		vector<vector<int>> soalArray(9);
		for(int i = 0 ; i<9 ; i++)
		{
			soalArray[i] = vector<int>(9);
			for(int j = 0 ; j<9 ; j++)
				soalArray[i][j] = 0;
		}

		string line[9];
		ifstream file;
		int count=0;
		file.open("sudoku.txt", ios::in);
		if(file.is_open())
		{
			while(getline(file,line[count]))
			{
				for(int i = 0 ; i<9 ; i++)
				{
					soalArray[i][count] = line[count][i]-'0';
					gridArray[i][count] = line[count][i]-'0';
				}
				cout<<endl;
				count++;
			}
			file.close();
		}
		while (window.isOpen())
		{
			sf::Event evnt;
			while (window.pollEvent(evnt))
			{
				if (evnt.type == evnt.Closed)
					window.close();
			}

			if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				indicator.setOutlineThickness(5);
				indicator.setOutlineColor(sf::Color::Blue);
				sf::Vector2i mousePos = sf::Mouse::getPosition(window);
				gridX = mousePos.x / 60;
				gridY = (mousePos.y - board.getPosition().y) / 60;
				cout<<gridX<<" , "<<gridY<<endl;
				if(gridX >= 0 && gridX <= 8 && gridY >= 0 && gridY <= 8)
					indicator.setPosition((float(gridX)*60.0f)+0.0f, (float(gridY)*60.0f)+160.0f);
			}

			if(gridX >= 0 && gridX <= 8 && gridY >= 0 && gridY <= 8)
			{
				if(text[gridX][gridY].getColor() != sf::Color::Black)
				{
					if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num1))
					{
						gridArray[gridX][gridY] = 1;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
					{
						gridArray[gridX][gridY] = 2;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num3))
					{
						gridArray[gridX][gridY] = 3;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num4))
					{
						gridArray[gridX][gridY] = 4;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num5))
					{
						gridArray[gridX][gridY] = 5;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num6))
					{
						gridArray[gridX][gridY] = 6;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num7))
					{
						gridArray[gridX][gridY] = 7;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num8))
					{
						gridArray[gridX][gridY] = 8;
					}
					else if(sf::Keyboard::isKeyPressed(sf::Keyboard::Num9))
					{
						gridArray[gridX][gridY] = 9;
					}
				}
			}
			
			for(int j = 0 ; j<9 ; j++)
			{
				for(int i = 0 ; i<9 ; i++)
				{
					if(gridArray[i][j] != 0)
					{
						text[i][j].setPosition((float(i)*60.0f)+15.0f, (float(j)*60.0f)+170.0f);
						text[i][j].setString(itoa(gridArray[i][j], temp, 10));	
					}
					if(soalArray[i][j] != 0)
					{
						text[i][j].setPosition((float(i)*60.0f)+15.0f, (float(j)*60.0f)+170.0f);
						text[i][j].setString(itoa(soalArray[i][j], temp, 10));
						text[i][j].setColor(sf::Color::Black);
					}
				}
			}
			
			window.clear();
			window.draw(board);
			window.draw(indicator);
			for(int j = 0 ; j<9 ; j++)
			{
				for(int i = 0 ; i<9 ; i++)
				{
					if(gridArray[i][j] != 0)
						window.draw(text[i][j]);
				}
			}
			window.draw(quit);
			window.display();
			int cek=0;

			if(!zerofound(gridArray))
			{
				for(int i = 0 ; i<9 ; i++)
				{
					for(int j = 0 ; j<9 ; j++)
					{
						int num = gridArray[i][j];
						if(!used_horizontal(gridArray,i,j,num) && !used_vertical(gridArray,i,j,num) && !used_region(gridArray,region,i,j,num))
						{
							text[i][j].setColor(sf::Color::Black);
							cek++;
							if(cek == 81) goto end;
						}
						else
						{
							text[i][j].setColor(sf::Color::Red);
						}
					}
				}
				
			}
			
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
			{ 
				goto menu;
			}
			
		}
	}
solveRandom:
	{
		for(int i = 0 ; i<9 ; i++)
		{
			gridArray[i] = vector<int>(9);
			for(int j = 0 ; j<9 ; j++)
				gridArray[i][j] = 0;
		
		}
		for(int j = 0 ; j<9 ; j++)
			{
				for(int i = 0 ; i<9 ; i++)
				{
					if(gridArray[i][j] != 0)
					{
						text[i][j].setPosition((float(i)*60.0f)+15.0f, (float(j)*60.0f)+170.0f);
						text[i][j].setString(itoa(gridArray[i][j], temp, 10));	
					}
					
				}
			}
		if(solve(gridArray, region, 0, 0, temp, text, quit, board, window) == false)
		{
			cout<<"No solution exists"<<endl;
		}
		else
		{
			goto end;
		}
	}

solveSpecial:
	{
		string line[9];
		ifstream file;
		int count=0;
		file.open("sudoku.txt", ios::in);
		if(file.is_open())
		{
			while(getline(file,line[count]))
			{
				for(int i = 0 ; i<9 ; i++)
					gridArray[i][count] = line[count][i]-'0';
				cout<<endl;
				count++;
			}
			file.close();
		}
		while (window.isOpen())
		{
			sf::Event evnt;
			while (window.pollEvent(evnt))
			{
				if (evnt.type == evnt.Closed)
					window.close();
			}

			for(int j = 0 ; j<9 ; j++)
			{
				for(int i = 0 ; i<9 ; i++)
				{
					if(gridArray[i][j] != 0)
					{
						text[i][j].setPosition((float(i)*60.0f)+15.0f, (float(j)*60.0f)+170.0f);
						text[i][j].setString(itoa(gridArray[i][j], temp, 10));	
					}
					
				}
			}

			if(solve(gridArray, region, 0, 0, temp, text, quit, board, window) == false)
			{
				cout<<"No solution exists"<<endl;
				goto menu;
			}
			else
			{
				goto end;
			}

		}

		return 0;
	}
end:
	{
		while(!sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			window.clear();
			window.draw(board);
			window.draw(indicator);
			for(int j = 0 ; j<9 ; j++)
			{
				for(int i = 0 ; i<9 ; i++)
				{
					window.draw(text[i][j]);
				}
			}
			window.draw(coba);
			window.display();
		}
		for(int i = 0 ; i<9 ; i++)
		{
			for(int j = 0 ; j<9 ; j++)
			{
				cout<<gridArray[i][j]<<" ";
			}
			cout<<endl;
		}
		
	}
}
